import Groq from 'groq-sdk';
import OpenAI from 'openai';
import { HookResponse } from '../shared/HookResponse.js';

const PROMPT = `
You will be presented with the text of a Wikipedia article. From this you must extract an interesting, concise and compelling hook.

The hook should complete the sentence starting with "Did you know..." with an interesting fact.

Hooks must meet the following guidelines:

 - Hooks must adopt a neutral point of view. Hooks that unduly focus on negative aspects of living persons should be avoided. 
 - The hook should be likely to be perceived as unusual or intriguing by readers with no special knowledge or interest. The most interesting hooks are the ones that leave the reader wanting to know more. At the same time, excessively sensational or gratuitous hooks should be rejected. Make sure to provide any necessary context for your hook – don't assume everyone worldwide is familiar with your subject. However, do keep hooks short and to the point. 
 - Hooks should not be facts that are widely known. They should be unexpected, unique, outrageous, poignant or exciting.
 - The hook cannot exceed 200 prose characters
 - Hooks must be supported by the text of the Wikipedia article.
 - Hooks must be one sentence long and not include emoji.
 - Hooks can omit uninteresting details if that makes it more punchy or interesting.
 - Hooks should not just summarize the article, they should provide an interesting detail from it.
 
Some good examples of hooks include:
 - ... that the fall of the Berlin Wall was the result of a bureaucratic mistake? 
 - ... that the Silver Cross Tavern is the United Kingdom's only legal brothel? 
 - ... that the United States once sued 50,000 cardboard boxes and clacker balls? 
 - ... that bodybuilding in China was once banned? 
 - ... that the tasty "chocolate pudding fruit" is used as a fish poison when unripe?
 
Your output should ONLY be a string starting with "...that" and ending with a question mark. Do not enclose the sentence in quotes.
 
The Wikipedia article follows

`;

async function generateHook(
  groq: Groq,
  openai: OpenAI,
  title: string
): Promise<HookResponse> {
  const articleUrl = `https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=wikitext&disabletoc=1&page=${title}`;
  const response = await fetch(articleUrl);
  const article = await response.json();
  if (article?.error) {
    return { error: article.error.info };
  }
  const text = article.parse.wikitext['*'];

  const completion = await openai.chat.completions.create({
    messages: [
      {
        role: 'system',
        content: PROMPT,
      },
      {
        role: 'user',
        content: text,
      },
    ],
    model: 'gpt-4o-2024-08-06',
    n: 5,
  });

  return {
    hooks: completion.choices
      .map((choice) => choice.message.content)
      .filter((hook) => hook !== null),
  };
}

export default generateHook;
