import * as cheerio from 'cheerio';

const API_URL =
  'https://magnus-toolserver.toolforge.org/commonsapi.php?meta&image=';

interface ImageInfo {
  name: string;
  width: number;
  height: number;
  author: string;
  licenses: string[];
  description: string;
}

const BANNED_ATTRIBUTION_STRINGS = [
  'not specified',
  'unknown author',
  'not provided',
  'not researched',
];

const STRIPPED_ATTRIBUTION_STRINGS = ['Attributed to'];

const STRIPPED_LICENSE_STRINGS = ['-migrated-with-disclaimers'];

function cleanAuthor(author: string | undefined): string | undefined {
  BANNED_ATTRIBUTION_STRINGS.forEach((bannedString) => {
    if (author && author.toLowerCase().includes(bannedString.toLowerCase())) {
      author = undefined;
    }
  });
  if (author) {
    STRIPPED_ATTRIBUTION_STRINGS.forEach((strippedString) => {
      author = author?.replace(strippedString, '');
    });
    const userUrlRegex = /User:(.*)/;
    const match = author.match(userUrlRegex);
    if (match) {
      author = match[1];
    }
  }

  // strip timestamps of the form 01:17, 10 March 2017 (UTC)
  const timestampRegex = /(\d{2}:\d{2}, \d{1,2} \w+ \d{4} \(UTC\))/;
  author = author?.replace(timestampRegex, '');

  return author;
}

function cleanLicense(license: string | undefined): string | undefined {
  if (!license) {
    return undefined;
  }
  if (license.toLowerCase().startsWith('pd')) {
    return 'Public Domain';
  }

  STRIPPED_LICENSE_STRINGS.forEach((strippedString) => {
    license = license?.replace(strippedString, '');
  });

  return license;
}

export async function getImageInfo(
  imageName: string
): Promise<ImageInfo | null> {
  const response = await fetch(`${API_URL}${imageName}`);
  if (!response.ok) {
    throw new Error(`Error fetching image info: ${response.statusText}`);
  }
  const xmlText = await response.text();
  const $ = cheerio.load(xmlText, { xmlMode: true });

  if ($('error').length) {
    return null;
  }

  // get a list of license names
  const licenseNameElements = $('license name');
  const licenseNames = licenseNameElements
    .map((_, element) => $(element).text())
    .get();

  const imageInfo: ImageInfo = {
    name: $('file name').text(),
    width: parseInt($('width').text(), 10),
    height: parseInt($('height').text(), 10),
    author: $('author').text(),
    licenses: licenseNames,
    description: $('description language').text(),
  };
  return imageInfo;
}

export function getAttributionString(imageInfo: ImageInfo): string {
  let license: string | undefined = imageInfo.licenses[0];
  if (imageInfo.licenses.some((l) => l.startsWith('PD'))) {
    license = 'PD';
  } else {
    let ccLicense = imageInfo.licenses.find((l) => l.startsWith('CC-BY'));
    if (ccLicense) {
      license = ccLicense;
    }
  }
  license = cleanLicense(license);

  let author: string | undefined = imageInfo.author;
  // author is often HTML. We want to extract the text
  const $ = cheerio.load(author);
  author = $.text();
  if (author.length > 50) {
    // check if there is a div.commons-creator element
    const $creator = cheerio.load(author);
    const creatorElement = $('span#creator');
    let newAuthor = author;
    if (creatorElement.length > 0) {
      newAuthor = creatorElement.text();
      if (newAuthor.length > 50) {
        newAuthor = $('span#creator a').text();
      }
    }
    author = newAuthor;
  }
  author = cleanAuthor(author);
  if (!author) {
    return license ? license.trim() : '';
  } else {
    return `${author.trim()}${license ? `, ${license.trim()}` : ''}`;
  }
}
