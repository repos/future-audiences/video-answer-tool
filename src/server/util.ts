import fs from 'fs';

export const NIGHTCORE_SPEED = 1.25;

export function getAssetPathPrefix() {
  // If running in production, use the /workspace/dist folder
  // If running in dev use the ./public folder
  if (process.env.NODE_ENV === 'production') {
    return '/data/scratch/video-answer-tool';
  } else if (process.env.NODE_ENV === 'staging') {
    return '/data/scratch/video-answer-tool-staging';
  } else {
    // create the public folder if it doesn't exist
    if (!fs.existsSync('./public')) {
      fs.mkdirSync('./public');
    }
    return './public';
  }
}

export function makeAssetFolders() {
  const assetPathPrefix = getAssetPathPrefix();
  const assetFolders = ['images', 'videos', 'audio', 'transcripts'];
  assetFolders.forEach((folder) => {
    const folderPath = `${assetPathPrefix}/${folder}`;
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath);
    }
  });
}

export function readDykJson() {
  const dykJson = fs.readFileSync('src/dyk.json', 'utf8');
  const res = JSON.parse(dykJson);
  return res;
}
