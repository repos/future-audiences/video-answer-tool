import 'dotenv/config';

import express from 'express';
import ViteExpress from 'vite-express';
import basicAuth from 'express-basic-auth';
import OpenAI from 'openai';
import Groq from 'groq-sdk';
import generateAnswer from './generateAnswer.js';
import dykAnswer, { dykCache, genDykAnswer } from './dykAnswer.js';
import bodyParser from 'body-parser';
import uploadVideo from './uploadVideo.js';
import { VideoRequest } from '../shared/VideoRequest.js';
import generateVideo from './generateVideo.js';
import fs from 'fs';
import { makeAssetFolders, getAssetPathPrefix, readDykJson } from './util.js';
import generateHook from './generateHook.js';
import recordVideo from './recordVideo.js';
import { ElevenLabsClient } from 'elevenlabs';
import { findCommonsImages } from './findCommonsImages.js';
import editTranscript from './editTranscript.js';
import { Answer, AnswerSegment } from '../shared/Answer.js';
import farmhash from 'farmhash';
import { MetaData } from '../client/types.js';

const openai = new OpenAI();
const groq = new Groq();
const elevenlabs = new ElevenLabsClient();
makeAssetFolders();

const app = express();
app.use(['/question', '/video'], bodyParser.urlencoded({ extended: false }));
app.use(['/question', '/video', '/edit'], bodyParser.json());

app.post('/question', (req, res) => {
  const question = req.body.question;
  const saveImagesLocally = req.body.saveImagesLocally || false;
  try {
    generateAnswer(groq, openai, question, saveImagesLocally).then((answer) => {
      console.log('Answer generated!');
      console.log(answer);
      res.json(answer);
    });
  } catch (error) {
    console.error('Error generating answer', error);
    res.status(500).send('Error generating answer');
  }
});

app.get('/dyk', (req, res) => {
  const factId = req.query.factId as string;
  const reroll = req.query.reroll === 'true';
  dykAnswer(Number.parseInt(factId), reroll, groq, elevenlabs, openai).then(
    (answer) => {
      console.log('DYK answer generated!');
      console.log(answer);
      res.json(answer);
    }
  );
});

app.get('/dyk-custom', (req, res) => {
  const articleTitle = req.query.articleTitle as string;
  const hook = req.query.hook as string;

  let factId = farmhash.fingerprint32(articleTitle + hook);
  if (factId > 0 && factId < 1000) {
    factId += 1000;
  }
  const transcriptPath = `${getAssetPathPrefix()}/transcripts/${factId}.json`;

  return genDykAnswer(
    articleTitle,
    hook,
    factId,
    groq,
    elevenlabs,
    openai
  ).then((answer) => {
    if ('errorMessage' in answer) {
      res.status(500).send(answer.errorMessage);
    } else {
      fs.writeFileSync(transcriptPath, JSON.stringify(answer, null, 2));
      res.json(answer);
    }
  });
});

app.post('/upload', express.raw({ type: '*/*', limit: '1gb' }), (req, res) => {
  const buffer = req.body;
  console.log(buffer);

  uploadVideo(buffer).then((url) => {
    console.log('Video generated!');
    res.json({ url });
  });
});

app.post('/edit', async (req, res) => {
  const factId = parseInt(req.query.factId as string, 10);
  if (isNaN(factId)) {
    res.status(400).send('Invalid factId');
    return;
  }

  const transcriptEdit = req.body.transcriptEdit as AnswerSegment[];
  const updatedMeta = req.body.meta as MetaData;

  if (!transcriptEdit) {
    res.status(400).send('Invalid transcriptEdit');
    return;
  }
  const answer = await editTranscript(
    factId,
    transcriptEdit,
    updatedMeta,
    elevenlabs,
    openai
  );
  res.json(answer);
});

/**
 * curl command to hit /edit endpoint at http://localhost:3000/

curl -X POST -H "Content-Type: application/json" -d '{
  "transcriptEdit": {
    "segments": [
      {
        "text": "Hello, this is the first segment"
      },
      {
        "text": "Hello, this is the second segment"
      },
      {
        "text": "Hello, this is the third segment"
      }
    ]
  }' "http://localhost:3000/edit?factId=1"
 */

app.post('/video', (req, res) => {
  const { videoId, soundFileIds } = req.body as VideoRequest;
  // Check that the file exists
  const videoFilepath = `${getAssetPathPrefix()}/videos/${videoId}.webm`;
  if (!fs.existsSync(videoFilepath)) {
    throw new Error(`Video file ${videoFilepath} not found`);
  }

  const soundFileBasePath = `${getAssetPathPrefix()}/audio/`;
  const soundFileExtension = '.mp3';
  const soundFilePaths = soundFileIds.map(
    (soundFileId) => `${soundFileBasePath}${soundFileId}${soundFileExtension}`
  );
  for (const soundFilePath of soundFilePaths) {
    if (!fs.existsSync(soundFilePath)) {
      throw new Error(`Sound file ${soundFilePath} not found`);
    }
  }
  generateVideo(videoId, videoFilepath, soundFilePaths).then((videoUrl) => {
    console.log('Video generated!');
    res.json({ videoUrl });
  });
});

app.get('/hook/:title', (req, res) => {
  const title = req.params.title;

  generateHook(groq, openai, title).then((hookResponse) => {
    console.log('Hook generated!');
    console.log(hookResponse);
    res.json(hookResponse);
  });
});

app.get('/record', async (req, res) => {
  const factId = req.query.factId as string;
  let tmpFile = await recordVideo(factId);
  if (!tmpFile) {
    res.status(500).send('Error recording video');
    return;
  }
  // set the proper headers
  res.setHeader('Content-Disposition', `attachment; filename=${factId}.mp4`);
  res.sendFile(tmpFile);
  res.on('finish', () => {
    fs.unlinkSync(tmpFile);
  });
});

if (process.env.USE_AUTH) {
  app.use(
    basicAuth({
      users: {
        admin: process.env.ADMIN_PASSWORD || 'admin',
        guest: process.env.GUEST_PASSWORD || 'guest',
      },
      challenge: true,
    })
  );
}

app.use('/static', express.static(getAssetPathPrefix()));

const port = parseInt(process.env.PORT || '3000', 10);
ViteExpress.listen(app, port, () =>
  console.log(`Server is listening on port ${port}...`)
);
