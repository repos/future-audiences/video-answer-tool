import fs from 'fs';
import { getAssetPathPrefix } from './util.js';

async function uploadVideo(videoDataBlob: any): Promise<string> {
  const uid =
    Math.random().toString(36).substring(2, 15) +
    Math.random().toString(36).substring(2, 15);
  const filepath = `${getAssetPathPrefix()}/videos/${uid}.webm`;
  fs.writeFileSync(filepath, videoDataBlob);
  return `/videos/${uid}.webm`;
}

export default uploadVideo;
