import Groq from 'groq-sdk';
import OpenAI from 'openai';
import html2plaintext from 'html2plaintext';
import fs from 'fs';
import { Answer } from '../shared/Answer.js';
import { ErrorResponse } from '../shared/ErrorResponse.js';
import mp3Duration from 'mp3-duration';
import { Readable } from 'stream';
import { finished } from 'stream/promises';
import { sleep } from '../shared/sleep.js';
import { getAssetPathPrefix, readDykJson, NIGHTCORE_SPEED } from './util.js';
import util from 'util';

import { getAttributionString, getImageInfo } from './commonsImageInfo.js';
import { ElevenLabs, ElevenLabsClient } from 'elevenlabs';
import { findCommonsImages } from './findCommonsImages.js';
import { ImageMeta } from './imageMeta.js';
import { url } from 'inspector';
import { exec as execCallback } from 'child_process';

const exec = util.promisify(execCallback);
let TTS_MODE = process.env.TTS_MODE || 'openai';

const PROMPT = `
You will be presented with a fun fact and the text of a related Wikipedia article.

Your task is to produce a video transcript that explains the fun fact in a way that is engaging and easy to understand.
You should start with a restatement of the fun fact, then provide some context or background information.
Restrict the transcript to three statements and corresponding images.

You will also be provided a list of image ids and their alt text. You should select an image that best fits the each line of the content in your transcript. Only return the ID of the image you want to use. Do not output IDs that are not mentioned.

Keep the tone light. Highlight unusual or unexpected elements of the subject that people will find interesting. Target a high-school reading level. No yapping. Be targeted and concise. Keep sentences short.
Don't say obvious things that everyone knows. Try not reuse images. Use only content from the article (don't invent new facts).

Output your response in JSON using the following format:
  {"segments": [{"text": string, "image": number}, {"text": string, "image": number}, {"text": string, "image": number}]}.

`;

export default async function dykAnswer(
  factId: number,
  reroll: boolean,
  groq: Groq,
  eleven: ElevenLabsClient,
  openai: OpenAI
): Promise<Answer | ErrorResponse> {
  if (Number.isInteger(factId) === false) {
    return { errorMessage: 'factId is not an integer' };
  }

  const pregenTranscript = getTranscript(factId);
  if (pregenTranscript && !reroll) {
    return pregenTranscript;
  }

  const facts: any[] = readDykJson();
  const articleTitle = facts[factId].title;
  const dykFact = facts[factId].fact;

  const transcriptPath = `${getAssetPathPrefix()}/transcripts/${factId}.json`;

  const answer = await genDykAnswer(
    articleTitle,
    dykFact,
    factId,
    groq,
    eleven,
    openai
  );

  if ('errorMessage' in answer) {
    return answer;
  } else {
    fs.writeFileSync(transcriptPath, JSON.stringify(answer, null, 2));
    return answer;
  }
}

export async function genDykAnswer(
  seedArticleTitle: string,
  dykFact: string,
  factId: number,
  groq: Groq,
  eleven: ElevenLabsClient,
  openai: OpenAI
): Promise<Answer | ErrorResponse> {
  const commonsImagesPromise = findCommonsImages(seedArticleTitle);
  const transformedSeedArticleTitle = seedArticleTitle.replace(/ /g, '_');
  const articleUrl = `https://en.wikipedia.org/w/api.php?action=parse&format=json&prop=text&&disabletoc=1&redirects=1&page=${transformedSeedArticleTitle}`;
  const response = await fetch(articleUrl);
  const articleData = await response.json();

  const articleText = articleData.parse.text['*'];
  // we need to update the title due to redirects
  const articleTitle = articleData.parse.title;
  const images: string[] = articleText.match(/<img[^>]+>/g) || [];
  const imageBaseUrl = `https://en.wikipedia.org/w/index.php?title=Special:Redirect/file/File:`;
  const imageAltTexts: string[] = [];
  const imageUrls: string[] = [];
  const imageMap: Record<string, string> = {};
  const imageNames: string[] = [];

  console.log(`Found images: ${JSON.stringify(images)}`);

  const bannedImageSubStrings = [
    'manul_distribution', // this image is manually banned for including India
    'icon',
    'rest_v1',
    'fig_',
    'fig-',
    'thumbnail.jpg',
    'osm-intl',
    '.tif',
    '.svg',
    '/wikipedia/en', // ban images from English Wikipedia (not Commons) because they are often non-free
  ];
  await images.map(async (image: string) => {
    const altTextMatches = image.match(/alt="([^"]+)"/);
    let altText = (altTextMatches && altTextMatches[1]) || '';

    const imageUrlMatches = image.match(/src="([^"]+)"/);
    const imageUrl = (imageUrlMatches && imageUrlMatches[1]) || '';
    for (const bannedImageSubString of bannedImageSubStrings) {
      if (imageUrl.toLowerCase().includes(bannedImageSubString.toLowerCase())) {
        return;
      }
    }
    // Base url: https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Donald_Trump_official_portrait_%28cropped%29.jpg/113px-Donald_Trump_official_portrait_%28cropped%29.jpg
    // Target url: https://en.wikipedia.org/wiki/Donald_Trump#/media/File:Donald_Trump_official_portrait_(cropped).jpg
    const fileName = imageUrl.split('/').pop();
    let cleanedFileName = fileName;
    if (cleanedFileName?.indexOf('px-') !== -1) {
      cleanedFileName = cleanedFileName!.split('px-')[1];
    }

    const cleanedImageUrl = `${imageBaseUrl}${cleanedFileName}`;
    if (!altText || `${altText}` === 'null' || `${altText}` === 'undefined') {
      altText = cleanedImageUrl.split('File:')[1];
    }

    imageAltTexts.push(altText);
    imageUrls.push(cleanedImageUrl);
    imageMap[altText] = altText;
    imageNames.push(cleanedFileName);
  });

  const commonsImages = await commonsImagesPromise;

  for (const commonsImage of commonsImages) {
    const urlEncodedImageName = encodeURIComponent(commonsImage);
    if (imageNames.includes(urlEncodedImageName)) {
      continue;
    }

    let bannedImage = false;
    for (const bannedImageSubString of bannedImageSubStrings) {
      if (
        commonsImage.toLowerCase().includes(bannedImageSubString.toLowerCase())
      ) {
        bannedImage = true;
        break;
      }
    }

    if (bannedImage) {
      continue;
    }
    const imageUrl = `https://commons.wikimedia.org/wiki/Special:Redirect/file/${commonsImage}`;
    imageAltTexts.push(commonsImage);
    imageUrls.push(imageUrl);
    imageNames.push(commonsImage);
    imageMap[commonsImage] = imageUrl;
  }

  if (Object.keys(imageMap).length === 0) {
    console.error(`No selectable images found in article: ${articleTitle}`);

    const defaultImageUrl =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/No-Image-Placeholder-landscape.svg/2560px-No-Image-Placeholder-landscape.svg.png';
    imageAltTexts.push('No Image Available');
    imageUrls.push(defaultImageUrl);
    imageNames.push('No Image Available');
    imageMap['No Image Available'] = defaultImageUrl;
  }

  const imageSelectionText = Object.keys(imageMap)
    .map((imageAlt, index) => `${index}: "${imageAlt}"`)
    .join(', ');
  console.log(`Using image selection text: ${imageSelectionText}`);

  const plainText = html2plaintext(articleText);

  const completion = await openai.chat.completions.create({
    response_format: { type: 'json_object' },
    messages: [
      {
        role: 'system',
        content: PROMPT,
      },
      {
        role: 'user',
        content: 'Fact: ' + dykFact,
      },
      {
        role: 'user',
        content: 'Wikipedia Article: \n' + plainText,
      },
      {
        role: 'user',
        content: 'Images: \n' + imageSelectionText,
      },
    ],
    model: 'gpt-4o',
  });
  const result = completion.choices[0].message.content;
  const resultJson = JSON.parse(result || '{}');
  let segments = resultJson.segments;

  if (
    segments.some(
      (segment) => segment.image < 0 || segment.image >= imageNames.length
    )
  ) {
    console.error(
      `Invalid image index in segments: ${JSON.stringify(segments)}`
    );
    console.error(`Images: ${imageSelectionText}`);
    return { errorMessage: 'Invalid image index' };
  }
  const selectedImageUrls = segments.map((segment) => {
    return imageUrls[segment.image] || '';
  });
  const speechFilePaths: string[] = ['', '', ''];
  const speechFileUrls: string[] = ['', '', ''];
  const speechFileDurations: number[] = [0, 0, 0];
  const alignments: any[] = [{}, {}, {}];

  const audioPromises = [];

  for (let segmentIndex = 0; segmentIndex < segments.length; segmentIndex++) {
    const segment = segments[segmentIndex];
    const text = segment.text;
    const audioPromise = renderAudio(text, factId, eleven, openai).then(
      (audioResult) => {
        speechFilePaths[segmentIndex] = audioResult.audioFilePath;
        speechFileUrls[segmentIndex] = audioResult.audioFileUrl;
        alignments[segmentIndex] = audioResult.alignments;
        speechFileDurations[segmentIndex] = audioResult.audioDuration;
      }
    );
    audioPromises.push(audioPromise);
  }

  const imageAttributions: string[] = await Promise.all(
    segments.map(async (segment: any) => {
      const imageName = imageNames[segment.image] || '';
      const imageInfo = await getImageInfo(imageName);
      if (imageInfo) {
        return getAttributionString(imageInfo);
      } else {
        console.error(`Failed to get image info for ${imageName}`);
        return '';
      }
    })
  );
  await Promise.all(audioPromises);

  const transcriptJson = {
    factId: factId,
    segments: segments.map((segment, index) => {
      //const character_end_times_seconds: number[] = alignments[index].character_end_times_seconds;
      return {
        title: articleTitle,
        text: segment.text,
        imageUrl: selectedImageUrls[index],
        imageAttribution: imageAttributions[index],
        speechFileUrl: speechFileUrls[index],
        speechFileDuration: speechFileDurations[index] + 0.5,
        //speechFileDuration: character_end_times_seconds[character_end_times_seconds.length - 1] + 0.5,
        //alignment: alignments[index],
      };
    }),
  };
  return transcriptJson;
}

export function dykCache(groq: Groq, eleven: ElevenLabsClient, openai: OpenAI) {
  const facts: any[] = readDykJson();
  for (let factId = 0; factId < 10; factId++) {
    try {
      dykAnswer(factId, groq, eleven, openai, false);
    } catch (error) {
      console.error(error);
    }
  }
}

export function getTranscript(factId: number) {
  const transcriptPath = `${getAssetPathPrefix()}/transcripts/${factId}.json`;
  if (fs.existsSync(transcriptPath)) {
    const transcriptJson = fs.readFileSync(transcriptPath, 'utf8');
    return JSON.parse(transcriptJson);
  }
  return null;
}

export interface AudioRenderResult {
  audioFilePath: string;
  audioFileUrl: string;
  audioDuration: number;
  alignments?: any;
}

export async function renderAudio(
  text: string,
  rngSeed: number,
  eleven: ElevenLabsClient,
  openai: OpenAI
): Promise<AudioRenderResult> {
  const uid =
    Math.random().toString(36).substring(2, 15) +
    Math.random().toString(36).substring(2, 15);
  const fileName = `${uid}.mp3`;
  const slowFileName = `${uid}_slow.mp3`;
  const speechFilePath = `${getAssetPathPrefix()}/audio/${fileName}`;
  const slowSpeechFilePath = `${getAssetPathPrefix()}/audio/${slowFileName}`;

  const speechFileUrl = `/audio/${fileName}`;

  let audioPromise;
  if (TTS_MODE === 'elevenlabs') {
    audioPromise = await elevenlabs_tts_timestamped(
      eleven,
      slowSpeechFilePath,
      text,
      rngSeed
    );
  } else if (TTS_MODE === 'openai') {
    audioPromise = openai_tts(openai, slowSpeechFilePath, text);
  } else {
    throw new Error(`Invalid TTS_MODE: ${TTS_MODE}`);
  }

  await audioPromise;

  // speed up audio using ffmpeg
  const command = `ffmpeg -i ${slowSpeechFilePath} -filter:a "atempo=${NIGHTCORE_SPEED}" ${speechFilePath}`;
  const { stdout, stderr } = await exec(command, { timeout: 300000 });
  const duration = await mp3Duration(speechFilePath);

  return {
    audioFilePath: speechFilePath,
    audioFileUrl: speechFileUrl,
    audioDuration: duration,
  };
}

function openai_tts(openai: OpenAI, fname: string, text: string) {
  const audioPromise = openai.audio.speech
    .create({
      model: 'tts-1',
      voice: 'alloy',
      input: text,
    })
    .then((response) => {
      return response.arrayBuffer();
    })
    .then((buffer) => {
      fs.writeFileSync(fname, Buffer.from(buffer));
    });
  return audioPromise;
}

async function elevenlabs_tts(
  elevenlabs: ElevenLabsClient,
  fname: string,
  text: string
) {
  const randomVoice =
    ELEVEN_LABS_VOICES[Math.floor(Math.random() * ELEVEN_LABS_VOICES.length)];
  const audioPromise = elevenlabs.textToSpeech.convert(randomVoice, {
    text: text,
    model_id: 'eleven_turbo_v2',
  });
  return audioPromise.then((response: Readable) => {
    const fileStream = fs.createWriteStream(fname);
    response.pipe(fileStream);
    return finished(fileStream);
  });
}

const ELEVEN_LABS_VOICES = [
  'ZF6FPAbjXT4488VcRRnw', // Amelia
  'vYENaCJHl4vFKNDYPr8y', // Riya
  '5e3JKXK83vvgQqBcdUol', // Health News Narrator (AKA Adam)
  'JBFqnCBsd6RMkjVDRZzb', // George
  'iP95p4xoKVk53GoZ742B', // Chris
  'SAz9YHcvj6GT2YYXdXww', // River
  'Xb7hH8MSUJpSbSDYk0k2', // Alice
];

async function elevenlabs_tts_timestamped(
  elevenlabs: ElevenLabsClient,
  fname: string,
  text: string,
  seed: number
) {
  const randomVoice =
    ELEVEN_LABS_VOICES[Math.floor(seed % ELEVEN_LABS_VOICES.length)];

  const audioPromise = elevenlabs.textToSpeech.convertWithTimestamps(
    randomVoice,
    {
      text: text,
      model_id: 'eleven_turbo_v2',
    }
  );
  return audioPromise.then((response) => {
    const audioResp: any = response;
    let audioB64 = audioResp.audio_base64;
    // keys: characters, character_start_times_seconds, character_end_times_seconds
    let alignment = audioResp.alignment;

    // Convert base64 to buffer
    let audioBuffer = Buffer.from(audioB64, 'base64');
    // write buffer to file
    fs.writeFileSync(fname, audioBuffer);
    return alignment;
  });
}
