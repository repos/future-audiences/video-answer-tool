import { ElevenLabsClient } from 'elevenlabs';
import { Answer, AnswerSegment } from '../shared/Answer.js';
import { ErrorResponse } from '../shared/ErrorResponse.js';
import { getTranscript, renderAudio } from './dykAnswer.js';
import { getAssetPathPrefix } from './util.js';
import { promises as fs } from 'fs';
import OpenAI from 'openai';
import { MetaData } from '../client/types.js';

export default async function editTranscript(
  factId: number,
  transcriptEdit: AnswerSegment[],
  updatedMeta: MetaData,
  eleven: ElevenLabsClient,
  openai: OpenAI
): Promise<Answer | ErrorResponse> {
  if (Number.isInteger(factId) === false) {
    return { errorMessage: 'factId is not an integer' };
  }
  const transcriptPath = `${getAssetPathPrefix()}/transcripts/${factId}.json`;
  const transcript: Answer | null = getTranscript(factId);

  if (transcript === null) {
    return { errorMessage: "Transcript can't be edited as it does not exist." };
  }

  const rerenderAudio: number[] = [];
  const originalSections = transcript.segments;
  // merge the two arrays replacing original keys with edited keys
  originalSections.forEach((section, index) => {
    // if the audio has been edited, add the index to the rerenderAudio array
    if (section.text !== transcriptEdit[index].text) {
      rerenderAudio.push(index);
    }
  });

  for (const index of rerenderAudio) {
    try {
      const text = transcriptEdit[index].text;
      const result = await renderAudio(text, factId, eleven, openai);
      transcriptEdit[index].speechFileUrl = result.audioFileUrl;
      transcriptEdit[index].speechFileDuration = result.audioDuration;
    } catch (error) {
      console.error('Error rendering audio', error);
      return { errorMessage: 'Error rendering audio' };
    }
  }
  const updatedTranscript = {
    meta: updatedMeta,
    segments: transcriptEdit,
  };

  // write the updated transcript to the file
  await fs.writeFile(
    transcriptPath,
    JSON.stringify(updatedTranscript, null, 2)
  );
  return updatedTranscript;
}
