const BASE_URL =
  'https://petscan.wmcloud.org/?wdf_main=1&doit=1&format=json&wdf_langlinks=1&manual_list_wiki=enwiki&manual_list=';

export async function findCommonsImages(pageName: string): Promise<string[]> {
  const response = await fetch(`${BASE_URL}${pageName}`);
  if (!response.ok) {
    throw new Error(`Error fetching image info: ${response.statusText}`);
  }
  const json = await response.json();

  const pages = json?.data;
  if (!pages) {
    return [];
  }

  const qid = Object.keys(pages)[0];
  if (!qid) {
    return [];
  }
  const images = Object.keys(pages[qid]);
  return images;
}
