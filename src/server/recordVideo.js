import puppeteer from 'puppeteer';
import { PuppeteerScreenRecorder } from 'puppeteer-screen-recorder';
import { getTranscript } from './dykAnswer';
import tmp from 'tmp';
import { getAssetPathPrefix } from './util';
import util from 'util';
import { exec as execCallback } from 'child_process';
import fs from 'fs';
import path from 'path';

// degree of slowdown during rendering for performance reasons
const renderSpeed = 0.5;
const exec = util.promisify(execCallback);

async function addAudio(outVideoFile, videoFilePath, audioFiles) {
  const audioInputCommand = audioFiles
    .map((soundFilePath) => `-i ${soundFilePath}`)
    .join(' ');

  const silenceLengthMs = 500;

  // unused knob for speeding up audio at this point
  const nightcoreSpeed = 1.0;

  const tempoCommand = `atempo=${nightcoreSpeed}`;
  const videoFilter = `-filter_complex "[0:v]setpts=${renderSpeed / nightcoreSpeed}*PTS[v]"`;

  const audioFilter = `-filter_complex "[1]adelay=0|0,pan=stereo|c0=c0|c1=c0,${tempoCommand}[a1]; [2]adelay=${silenceLengthMs}|${silenceLengthMs},pan=stereo|c0=c0|c1=c0,${tempoCommand}[a2]; [3]adelay=${silenceLengthMs}|${silenceLengthMs},pan=stereo|c0=c0|c1=c0,${tempoCommand}[a3]; [4]adelay=${silenceLengthMs}|${silenceLengthMs}[a4]; [a1][a2][a3][a4]concat=n=4:v=0:a=1"`;
  let command = `ffmpeg -y -r 60 -i ${videoFilePath} ${audioInputCommand} ${audioFilter} ${videoFilter} -map "[v]" -map 1:a -c:v libx264 -c:a aac ${outVideoFile}`;
  console.log(`Starting command: ${command}`);

  try {
    const { stdout, stderr } = await exec(command, { timeout: 300000 });
    console.log('encoding finished');
    console.log('stdout:', stdout);
    console.log('stderr:', stderr);
    return outVideoFile;
  } catch (e) {
    console.error('error encoding video', e);
    return null;
  }
}

export default async function recordVideo(factId) {
  const outVideoFile = path.resolve(
    getAssetPathPrefix() + `/videos/${factId}.mp4`
  );

  if (fs.existsSync(outVideoFile)) {
    console.log(`video already exists for fact ${factId}`);
    return outVideoFile;
  }

  const browser = await puppeteer.launch({
    headless: true,
    firefox: 'firefox',
    defaultViewport: {
      width: 1080,
      height: 1920,
    },
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  });
  const page = await browser.newPage();

  if (process.env.USE_AUTH) {
    await page.authenticate({
      username: 'admin',
      password: process.env.ADMIN_PASSWORD,
    });
  }
  const recorder = new PuppeteerScreenRecorder(page, { fps: 60 });
  await page.goto(
    `${process.env.ADDRESS}/facts/${factId}?render=true&speed=${renderSpeed}`,
    {
      waitUntil: 'domcontentloaded',
    }
  );
  await page.waitForFunction('window.WikiTok.ready === true');
  console.log(`start recording for fact ${factId}`);

  // get temp file path
  const tempFile = tmp.fileSync({ postfix: '.mp4' });
  const tempFilePath = tempFile.name;
  await recorder.start(tempFilePath);
  await page.waitForFunction('window.WikiTok.done === true', {
    timeout: 120 * 1000,
  });
  await recorder.stop();
  await browser.close();

  const transcript = getTranscript(factId);
  const audioFiles = transcript.segments.map(
    (t) => `${getAssetPathPrefix()}${t.speechFileUrl}`
  );

  audioFiles.push(`${getAssetPathPrefix()}/audio/logo.mp3`);

  const audioVideoPath = await addAudio(outVideoFile, tempFilePath, audioFiles);

  return audioVideoPath;
}
