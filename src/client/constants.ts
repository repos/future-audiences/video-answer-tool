import { InjectionKey } from 'vue';
import { MenuItemData } from '@wikimedia/codex';
import { Fact } from './types';

export const categories: MenuItemData[] = [
  { label: 'All categories', value: '' },
  { label: 'Art & Culture', value: 'art-culture' },
  { label: 'Science & Technology', value: 'science-technology' },
  { label: 'History & Biography', value: 'history-biography' },
  { label: 'Sports & Games', value: 'sports-games' },
  { label: 'Food & Drink', value: 'food-drink' },
  { label: 'Medicine & Health', value: 'medicine-health' },
  { label: 'Animals & Wildlife', value: 'animals-wildlife' },
  { label: 'Geography & Nature', value: 'geography-nature' },
  { label: 'Music & Entertainment', value: 'music-entertainment' },
  { label: 'Politics & Society', value: 'politics-society' },
  { label: 'Language & Literature', value: 'language-literature' },
  { label: 'Religion & Mythology', value: 'religion-mythology' },
];

export const videoData = {
  segments: [
    {
      text: "Contrary to what you'd expect, Niels Bohr's theory, often called the gunslinger effect, suggests that drawing first in a gunfight might actually lead to losing.",
      imageUrl:
        'https://en.wikipedia.org/w/index.php?title=Special:Redirect/file/File:Niels_Bohr_1935.jpg',
      speechFileUrl: '/audio/2rb522jyumo0srm69b6pys.mp3',
      speechFileDuration: 9.912,
    },
    {
      text: 'Bohr got this idea from watching Western movies, where a gunslinger wins by drawing second. He figured reactions could be faster than deliberate actions.',
      imageUrl:
        'https://en.wikipedia.org/w/index.php?title=Special:Redirect/file/File:Mexican_Standoff.jpg',
      speechFileUrl: '/audio/xlli18cf4tsupj4r6dz5.mp3',
      speechFileDuration: 9.624,
    },
    {
      text: 'Experiments showed that reaction speeds could be up to 10% faster than planned actions. But accuracy could suffer, making this advantage not as straightforward in real shootouts.',
      imageUrl:
        'https://en.wikipedia.org/w/index.php?title=Special:Redirect/file/File:Reaction_time_stages.png',
      speechFileUrl: '/audio/ql33jpgdvgkxbrz3dxqj27.mp3',
      speechFileDuration: 11.4,
    },
  ],
};

export const FactsKey: InjectionKey<Fact[]> = Symbol('Fact');
export const CategoriesKey: InjectionKey<MenuItemData[]> = Symbol('Categories');
