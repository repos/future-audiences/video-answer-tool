import { createApp } from 'vue';
import router from './router';
import '@wikimedia/codex/dist/codex.style.css';
import './style.css';
import App from './App.vue';

createApp(App).use(router).mount('#root');
