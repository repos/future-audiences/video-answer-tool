export interface Fact {
  id: number;
  title: string;
  fact: string;
  category: string;
}

export interface FactSegmentData {
  layout: 'standard' | 'small';
  title: string;
  text: string;
  imageAlignment:
    | 'top left'
    | 'top center'
    | 'top right'
    | 'center left'
    | 'center center'
    | 'center right'
    | 'bottom left'
    | 'bottom center'
    | 'bottom right';
  imageAttribution: string;
  imageUrl: string;
  speechFileDuration: number;
  speechFileUrl: string;
  imageElement?: HTMLImageElement;
}

export interface MetaData {
  ken: string;
  transition: string;
  color: 'white' | 'black' | 'blue';
  logoImageElement?: HTMLImageElement;
}

export interface DykResponse {
  segments: FactSegmentData[];
}
