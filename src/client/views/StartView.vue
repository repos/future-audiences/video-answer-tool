<template>
  <div class="start-view">
    <div class="start-view__header">
      <div class="start-view__header__title">
        <div class="start-view__header__new-fact">
          <cdx-button @click="router.push('/new')" variant="secondary"
            ><cdx-icon :icon="cdxIconAdd" /> New Fact</cdx-button
          >
        </div>
        <h1>Did you know?</h1>
      </div>
      <div class="start-view__header__categories">
        <cdx-select
          v-model:selected="selectedCategory"
          :menu-items="categories"
          default-label="Filter by category"
        />
      </div>
    </div>

    <div class="start-view__content">
      <div class="start-view__content__track">
        <div class="start-view__content__track__inner">
          <TransitionGroup>
            <Teaser
              v-for="fact in filteredFacts"
              :id="fact.id"
              :key="fact.id"
              class="start-view__content__teaser"
            />
          </TransitionGroup>
        </div>
      </div>
    </div>
  </div>
</template>

<script lang="ts" setup>
import { computed, ref, inject, onMounted, watch } from 'vue';
import { useRouter, useRoute } from 'vue-router';
import { FactsKey, CategoriesKey } from '../constants';
import { CdxSelect, CdxIcon, CdxButton } from '@wikimedia/codex';
import { cdxIconAdd } from '@wikimedia/codex-icons';

import Teaser from '../components/Teaser.vue';

const facts = inject(FactsKey)!;
const categories = inject(CategoriesKey)!;
const route = useRoute();
const router = useRouter();
const selectedCategory = ref('');

const filteredFacts = computed(() => {
  return facts.filter((fact) => {
    if (selectedCategory.value === '') {
      return true;
    } else {
      return fact.category === selectedCategory.value;
    }
  });
});

// clear the hash when the category changes
watch(selectedCategory, () => {
  router.replace({ hash: '' });
  document
    .querySelector('.start-view__content__teaser')
    ?.scrollIntoView({ behavior: 'smooth', inline: 'center', block: 'center' });
});

// If the user is returning from a previous Fact, scroll it into the center of
// the sequence so they can resume browsing from where they left off
onMounted(() => {
  if (route.hash) {
    document.querySelector(route.hash)?.scrollIntoView({
      behavior: 'smooth',
      inline: 'center',
      block: 'center',
    });
  }
});
</script>

<style>
.start-view {
  display: flex;
  flex-direction: column;
  height: 100%;
}

.start-view__header {
  display: grid;
  grid-template-columns: var(--grid);
}

.start-view__header__title {
  grid-column: content;
}

.start-view__header__new-fact {
  float: right;
  margin-top: 1rem;
}

.start-view__header__categories {
  grid-column: content;
}

.start-view__header__categories .cdx-select-vue,
.start-view__header__categories .cdx-select-vue__handle {
  width: 100%;
}

.start-view__content {
  flex-grow: 1;
  display: grid;
  grid-template-columns: var(--grid);
}

.start-view__content__track {
  grid-column: full;
  display: grid;
  grid-template-columns: inherit;
  padding-block: var(--space);
  overflow-x: scroll;
  overflow-y: hidden;
  overscroll-behavior-x: contain;
  scroll-snap-type: x mandatory;
  scrollbar-width: none;
}

.start-view__content__track__inner {
  grid-column: content;
  display: flex;
  align-items: center;
  gap: calc(var(--space) * 1);

  @media (min-width: 640px) {
    gap: calc(var(--space) * 3);
    margin-bottom: 2rem;
  }

  @media (min-width: 960px) {
    margin-bottom: 5rem;
  }
}

.start-view__content__track__inner::after {
  content: '';
  align-self: stretch;
  padding-inline-end: max(
    var(--space),
    (100vw - var(--content-max-width)) / 2 - var(--space)
  );
}
</style>
