import { createRouter, createWebHistory } from 'vue-router';
import StartView from './views/StartView.vue';
import FactView from './views/FactView.vue';
import NewFact from './views/NewFact.vue';

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: StartView,
    },
    {
      path: '/facts/:id',
      component: FactView,
      props: (route) => ({ id: Number(route.params.id) }),
    },
    {
      path: '/new',
      component: NewFact,
    },
  ],
});
