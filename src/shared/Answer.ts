export interface AnswerSegment {
  title: string;
  text: string;
  imageUrl: string;
  speechFileUrl: string;
  speechFileDuration: number;
}

export interface Answer {
  segments: AnswerSegment[];
  factId?: number;
}
