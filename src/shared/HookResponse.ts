export interface HookResponse {
  hooks?: string[];
  error?: string;
}
