# WMF Video Answer Tool

An experimental tool that auto-generates "explainer" videos based on existing Wikimedia content, suitable for social media.
This tool is a single-page web application with a Node.js back-end. A [dataset of community-curated trivia](https://huggingface.co/datasets/derenrich/enwiki-did-you-know) (["Did You Know?"](https://en.wikipedia.org/wiki/Wikipedia:Did_you_know)) from English Wikipedia is used to provide topics and initial hooks for the videos. AI tools (OpenAI, Groq, 11Labs) are used to find and summarize key facts from the topic, select and animate associated Commons images, and generate AI narration.

## Requirements

You need a recent version of Node.js to run the project locally (v18 or later
recommended). You also need a Toolforge account that has access to the
`video-answer-tool` project in order to get the necessary API keys.

For the server to run, you will need to have `ffmpeg` installed and reachable
over either the `FFMPEG_PATH` variable, or the binary must be in your `PATH`.

## Installation and setup

- Clone the repository
- Install the dependencies using `npm install`
- Copy the `.env.example` file to a new file called `.env`
- Add the `GROQ_API_KEY` and `OPENAI_API_KEY` environmental variables to your `.env` file
- Set a local `ADMIN_PASSWORD` in this file (this can be anything)
- Run the development server via `npm run dev`.

### Getting API Keys

To get the API keys for the 3rd-party services that this app relies on, you
should log in to toolforge, `become` the app, and run `toolforge envvars list`
from the command line. Here's a somewhat longer explanation of the steps to follow:

1. SSH into Toolforge using your WMF developer credentials: `ssh myusername@login.toolforge.org`.
   If you need help accessing Toolforge or setting up an SSH key there, see [this
   guide](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Quickstart) for more
   information.
2. From the SSH command line, run `become video-answer-tool`.
3. Once you have done this, you can run `toolforge envvars list` to see all the environmental vars
   currently in use. Copy the appropriate keys into your local environment and you should be
   good to go. `toolforge envvars show MY_VARIABLE` will give the full value of a given env var.
